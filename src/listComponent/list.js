import React, {useEffect, useState} from "react";
import ListItem from "./listItem";

export default function List(props) {
    const [ userCount, setUserCount ] = useState("");


    useEffect(() => {
        setUserCount(props.users.length);
    },[]);

    return(
        <ListItem 
        users = {props.users}
        userCount = {userCount}/>
    );
}