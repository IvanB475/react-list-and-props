import React from "react";


export default class ListItem extends React.Component {
    render() {
        const printOutUserInfo = this.props.users.map(user => <li key={user.name} >Name: {user.name}, Age: {user.age}, Date of registration: {user.registrationDate}</li>);
        return (
            <div>
            {printOutUserInfo}
            <h4>total user count: { this.props.userCount }</h4>
            </div>
        )
    }
}