import './App.css';
import { List } from './listComponent';

const users = [
  {name: 'John', age: 20, registrationDate: "20.8.2019"},
  {name: 'Jane', age: 34, registrationDate: "14.4.2021"},
  {name: 'Angel', age: 17, registrationDate: "10.12.2020"},
  {name: 'Randy', age: 45, registrationDate: "8.2.2008"},
  {name: 'Marcus', age: 54, registrationDate: "19.5.2015"}
]

function App() {
  return (
    <div className="App">
      <List users={users}></List>
    </div>
  );
}

export default App;
